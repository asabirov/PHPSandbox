<?php
namespace PHPSandbox;

use PhpParser\Node;
use \PhpParser\NodeVisitorAbstract;

class PatchVisitor extends NodeVisitorAbstract
{
    /**
     * @var PHPSandbox
     */
    private $sandbox;

    public function __construct(PHPSandbox $sandbox)
    {
        $this->sandbox = $sandbox;
    }

    /**
     * @param Node[] $nodes
     * @return Node|Node[]|void|null
     */
    public function afterTraverse(array $nodes)
    {
        foreach ($nodes as $index => $node) {
            if ($node instanceof Node\Stmt\Class_) {
                $this->makeFirst($nodes, $index);
            }
        }

        return $nodes;
    }

    /**
     * Move node with $index to in the beginning of the $nodes
     * @param $nodes
     * @param $a
     */
    private function makeFirst(&$nodes, $a)
    {
        $out = array_splice($nodes, $a, 1);
        array_splice($nodes, 0, 0, $out);
    }
}
